<?php
/*
Plugin Name: WooCommerce Report Modifier
Plugin URI: https://www.storelocatorplus.com/product/woocommerce-report-modifier/
Description: Adds an extended customer report that can be downloaded as a CSV file as well as other WooCommerce report updates.
Author: Store Locator Plus
Author URI: https://www.storelocatorplus.com
License: GPL3
Tested up to: 4.8
Version: 3.3

Text Domain: woocommerce-report-modifier
Domain Path: /languages

Copyright 2015 - 2017 Charleston Software Associates (info@charlestonsw.com)
*/

// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) || ! defined( 'ABSPATH' ) ) {
	exit;
}

// We do not do any heartbeat processing
if ( defined( 'DOING_AJAX' ) && DOING_AJAX && ! empty( $_POST[ 'action' ] ) && ( $_POST['action'] === 'heartbeat' ) ) {
	return;
}

require_once( 'include/base/WCReportModifier.php' );

/**
 * Main instance of WCReportModifier
 *
 * @return WCReportModifier
 */
function WCReportModifier() {
	if ( ! defined( 'WCReportModifier_FILE' ) ) {
		define( 'WCReportModifier_FILE' , __FILE__ );
	}
	if ( ! defined( 'WCReportModifier_PATH' ) ) {
		define( 'WCReportModifier_PATH' , plugin_dir_path( WCReportModifier_FILE ) );
	}
	return WCReportModifier::instance();
}

$GLOBALS['wcreportmodifier'] = WCReportModifier();

add_action( 'init', array( $GLOBALS['wcreportmodifier'] , 'init' ) );


// Password Reset Removal
if ( 'no' === get_option( 'wcrm_stop_password_notifications' ) ) {
	remove_action( 'after_password_reset', 'wp_password_change_notification' );
}

