=== WooCommerce Report Modifier ===
Plugin Name:  WooCommerce Report Modifier
Contributors: charlestonsw
Donate link: https://www.storelocatorplus.com/product/woocommerce-report-modifier/
Tags: woocommerce, reports, export, mailchimp
Requires at least: 4.4
Tested up to: 4.8
Stable tag: 3.3

Augment the WooCommerce reports to do more than what comes "out of the box".  Plus some other "Woo extras".  Requires WooCommerce 3.0 or higher.

== Description ==

Update the WooCommerce reports plus some other "Woo extras".

= New Reports =

Orders / Order Details

Created for our local volleyball club where all orders are for a single SKU such as "Tuesday Night League".
Every order contains multiple options which are the player names, tee-sizes, and contact details for the team.

* Group output by date , same as with WooCommerce graph reports.
* Shows order details including the order date, first line item SKU, and details on the variants an options for each line item.
* Can easily group the output by first-line-item SKU.

Customers / Extended List

* Export the current on-screen table to CSV
* Set the number of rows to show on each extended list report; remembered for each user
* Set which columns you want to show on the table
* Sort customer list by Orders (count) , Money Spent, Email, Username
* Actionable username field - click to view/edit user
* Actionable order count field - click to view user orders

= Modified Reports =

Orders / Sales By Product

*  show 30 products versus the standard limit of 12

= Order Details =

Augments the Order Details user details.

* Adds a direct link to "edit user"
* Adds a direct link to "switch user" if the [User Switching plugin](https://github.com/johnbillion/user-switching) is active

= Product Page Options

Turn off related products on all product pages with a simple checkbox setting.


== Installation ==

= Requirements =

* WordPress: 4.4
* WooCommerce: 3.0
* PHP: 5.2

= Install After WooCommerce =

1. Purchase this plugin from the [Store Locator Plus](https://www.storelocatorplus.com) website to get the latest .zip file.
2. Go to plugins/add new and select upload.
3. Upload the woocommerce-report-modifier.zip file and activate it.

== Frequently Asked Questions ==

= Where do I get support? =

Use the [Store Locator Plus Contact Form](https://www.storelocatorplus.com/contact-us/) to reach us by email.

= What are the terms of the license? =

The license is GPL.  You get the code, feel free to modify it as you
wish. I prefer that customers pay because they like what I do and
want to support the effort that brings useful software to market.  Learn more
on the [License Terms page](https://www.storelocatorplus.com/products/general-eula/).

== Changelog ==

= 3.3 =

* Fix username sorting on Customers Extended List report.
* Add customer ID with sorting to Customers Extended List report.  ID is basically synonomous with user sign up date which is useful for only adding latest users to MailChimp.
* Tested with WP 4.8

= 3.2 =

* Add order date to Order Details report.
* Add SKU to the product name in header for better grouping.
* Better handling of orders with multiple line items with order by SKU and Product Name.

= 3.1 =

* Add sort by product name to Order Details report.
* Update Order Details report with sort by submenu.
* Order ID now links to edit/view order.

= 3.0 =

* Updated to use the WooCommerce classes and objects which is a MAJOR UPGRADE and change to WooCommerce.
* Added the Order Details report.

= 2.0 =

* Updated to use WC 2.7 factory methods to get properties of orders.
* Updated to move PHP files into subdirs to reduce WP Core overhead of scanning for plugin headers.
* Add separated first/last name on extended customer list report for better exports for MailChimp.

= 1.6 =

Enhancements

* Add Order Details Edit User link - allows you to directly jump to the edit user page from the order details page.
* Add Order Details Switch User link - if [John Billion's User Switching plugin](https://github.com/johnbillion/user-switching) is active allows you to switch to the customer and switch back with one-click.


= 1.5 =

Enhancements

* Early exit on WP Heartbeat since we don't do any of that stuff.

= 1.4 =

Enhancements

* Extended Customer Report - Show separate first and last name. Provides better MailChimp import mapping for the downloaded CSV.
* Extended Customer Report - Show the last 20 orders on the order list column (replaces last_order).
* Extended Customer Report - Add ability to sort by Last Order, which sorts on the lastupdated stamp for the user.
* Supports WC 2.7 Update extended customer report to use WooCommerce special access methods get_id() and get_date_created() instead of directly accessing properties (pre WC 2.7).

Change

* Extended Customer Report - default sort is by most recently updated customer details first.

= 1.3 =

Enhancements

* Add checkbox on Email settings to stop sending password change notifications to site admins.

= 1.2 =

Enhancements

* Add option to turn of Related Products on WooCommerce pages.  Especially useful with the Storefront theme.
* Only load most of the report modifier code is user is_admin().   This is more efficient.

Fixes

* Use wcreportmodifier as global name not woocommerce.  We don't need any fights here.

= 1.1 =

Initial public release.