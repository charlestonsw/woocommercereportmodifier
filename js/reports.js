jQuery(function( $ ) {

    var a = document.createElement( 'a' );

    if ( typeof a.download === 'undefined' ) {
        $( '.export_csv_2' ).hide();
    }

    // Export
    $( '.export_csv_2' ).click( function() {
        var export_format = $( this ).data( 'export' );
        var csv_data      = 'data:application/csv;charset=utf-8,';

        if ( 'table' === export_format ) {

            $( this ).offsetParent().find( 'thead tr,tbody tr' ).each( function() {
                $( this ).find( 'th, td' ).each( function() {
                    if ( $( this ).is(':visible' ) ) {
                        var value = $(this).text();
                        value = value.replace('#', '');
                        value = value.replace('[?]', '');
                        csv_data += '"' + value + '"' + ',';
                    }
                });
                csv_data = csv_data.substring( 0, csv_data.length - 1 );
                csv_data += '\n';
            });

            $( this ).offsetParent().find( 'tfoot tr' ).each( function() {
                $( this ).find( 'th, td' ).each( function() {
                    if ( $( this ).is(':visible') ) {
                        var value = $(this).text();
                        value = value.replace('#', '');
                        value = value.replace('[?]', '');
                        csv_data += '"' + value + '"' + ',';
                        if ($(this).attr('colspan') > 0) {
                            for (i = 1; i < $(this).attr('colspan'); i++) {
                                csv_data += '"",';
                            }
                        }
                    }
                });
                csv_data = csv_data.substring( 0, csv_data.length - 1 );
                csv_data += '\n';
            });
        }

        // Set data as href and return
        $( this ).attr( 'href', encodeURI( csv_data ) );
        return true;
    });
});
