<?php

if ( ! class_exists( 'WCRM_Products' ) ) :
	require_once( WCReportModifier_PATH . 'include/base/WCRM_Object.php');

	/**
	 * Class WCRM_Products
	 *
	 * Text Domain: woocommerce-report-modifier
	 *
	 */
	class  WCRM_Products extends WCRM_Object  {

		/**
		 * Things we do when we start.
		 */
		public function initialize() {
			$this->init_for_all();
			$this->init_for_admin();
		}

		/**
		 * Things we do at start for admin users only.
		 */
		private function init_for_admin() {
			if ( ! is_admin() ) {
				return;
			}
			$this->add_related_products_checkbox_to_settings();
		}

		/**
		 * Things do at start for all users.
		 */
		private function init_for_all() {
			// Remove Related Products Option Is Enabled.
			//
			if ( 'yes' === get_option( 'wcrm_remove_related_products' ) ) {
				add_filter( 'woocommerce_related_products_args', array(
					$this,
					'remove_related_products_from_product_pages'
				), 10 );
			}
		}

		/**
		 * Set filter to add related products checkbox to WooCommerce products settings page.
		 */
		private function add_related_products_checkbox_to_settings() {
			add_filter( 'woocommerce_product_settings' , array( $this , 'woocommerce_product_settings_remove_related' ) );
		}

		/**
		 * Modify the product settings array with the woocommerce_product_settings filter.
		 *
		 * @param   array $settings
		 * @return  array
		 */
		public function woocommerce_product_settings_remove_related( $settings ) {

			$new_setting = array(
				'title'         => __( 'Remove Related Products', 'woocommerce-report-modifier' ),
				'desc'          => __( 'Remove related products from product pages.', 'woocommerce-report-modifier' ),
				'id'            => 'wcrm_remove_related_products',
				'default'       => 'no',
				'type'          => 'checkbox',
			);

			$new_settings = array();
			foreach ( $settings as $setting ) {
				if ( $setting['id'] === 'woocommerce_cart_redirect_after_add' ) {
					$new_settings[] = $new_setting;
				}
				$new_settings[] = $setting;
			}

			return $new_settings;
		}

		/**
		 * Remove related products from bottom of products pages.
		 *
		 * Especially useful with the Storefront theme.
		 *
		 * @param array $args
		 *
		 * @return array
		 */
		public function remove_related_products_from_product_pages( $args ) {
			return array();
		}
	}

	global $wcreportmodifier;
	if ( empty( $wcreportmodifier->objects['Products'] ) ) {
		$wcreportmodifier->objects['Products'] = new WCRM_Products();
	}

endif;
