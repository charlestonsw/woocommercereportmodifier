<?php
/**
 * Render the view for the Add On Details Report
 */
defined( 'ABSPATH' ) or exit;
?>

<div id="poststuff" class="woocommerce-reports-wide">
    <div class="postbox">

		<?php if ( 'custom' === $current_range && isset( $_GET['start_date'], $_GET['end_date'] ) ) : ?>
            <h3 class="screen-reader-text"><?php echo esc_html( sprintf( _x( 'From %s to %s', 'start date and end date', 'woocommerce-report-modifier' ), wc_clean( $_GET['start_date'] ), wc_clean( $_GET['end_date'] ) ) ); ?></h3>
		<?php else : ?>
            <h3 class="screen-reader-text"><?php echo esc_html( $ranges[ $current_range ] ); ?></h3>
		<?php endif; ?>

        <div class="stats_range">
            <ul>
				<?php
				foreach ( $ranges as $range => $name ) {
					echo '<li class="' . ( $current_range == $range ? 'active' : '' ) . '"><a href="' . esc_url( remove_query_arg( array( 'start_date', 'end_date' ), add_query_arg( 'range', $range ) ) ) . '">' . $name . '</a></li>';
				}
				?>
                <li class="custom <?php echo $current_range == 'custom' ? 'active' : ''; ?>">
					<?php _e( 'Custom:', 'woocommerce-report-modifier' ); ?>
                    <form method="GET">
                        <div>
							<?php
							// Maintain query string
							foreach ( $_GET as $key => $value ) {
								if ( is_array( $value ) ) {
									foreach ( $value as $v ) {
										echo '<input type="hidden" name="' . esc_attr( sanitize_text_field( $key ) ) . '[]" value="' . esc_attr( sanitize_text_field( $v ) ) . '" />';
									}
								} else {
									echo '<input type="hidden" name="' . esc_attr( sanitize_text_field( $key ) ) . '" value="' . esc_attr( sanitize_text_field( $value ) ) . '" />';
								}
							}
							?>
                            <input type="hidden" name="range" value="custom" />
                            <input type="text" size="11" placeholder="yyyy-mm-dd" value="<?php if ( ! empty( $_GET['start_date'] ) ) echo esc_attr( $_GET['start_date'] ); ?>" name="start_date" class="range_datepicker from" />
                            <span>&ndash;</span>
                            <input type="text" size="11" placeholder="yyyy-mm-dd" value="<?php if ( ! empty( $_GET['end_date'] ) ) echo esc_attr( $_GET['end_date'] ); ?>" name="end_date" class="range_datepicker to" />
                            <input type="submit" class="button" value="<?php esc_attr_e( 'Go', 'woocommerce-report-modifier' ); ?>" />
                        </div>
                    </form>
                </li>
            </ul>
        </div>
        <div class="inside">
            <div class="submenu">
                <div class="sort_options">
                    <span class="submenu_label"><?= __('Sort By:','woocommerce-report-modifier') ?></span>
                    <?php $this->display_sort_submenu(); ?>
                </div>
            </div>
            <table id="wrcm_addon_details" class="customer_list">
                <?php $this->output_addon_details(); ?>
            </table>
        </div>
    </div>
</div>
