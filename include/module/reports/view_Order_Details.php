<?php
$order_url = admin_url( 'post.php?post=' . $full_order['id'] . '&action=edit' );
$order_text =
    __( 'Order ' , 'woocommerce-report-modifier' ) .
    " <a href='{$order_url}' target='order_detail' title='{$this->reports[ 'id' ][ 'link_text' ]}'>{$full_order["id"]}</a> " .
    $post->post_title;
$order_text = str_replace( 'Order –' , '' , $order_text );
$order_text = str_replace( 'Order &ndash;' , '' , $order_text );

$order_id_class = ( $this->sort_by === 'id' )  ? '' : 'line_above';

if ( $current_header !== $full_order[ $this->sort_by ] ):
    $current_header = $full_order[ $this->sort_by ];
?>
<tr class="order_header" id="header-<?= $full_order['id'] ?>">
    <td class="sorted_by" colspan="6"><?= $full_order[ $this->sort_by ]?> <?= $order->get_date_completed() ?></td>
</tr>
<?php endif; ?>
<tr class="order_detail">
    <td class="order_id <?= $order_id_class ?>" colspan="6"><?= $order_text ?></td>
</tr>
<tr class="order_detail">
	<tbody class="order_line_items">
	<?php
	foreach ( $full_order['line_items'] as $item_id => $item ) {
	    if ( ( $this->sort_by !== 'id' ) && ( strpos( $idx_key, '-' . $item_id ) === false ) ) {
	        continue;
        }
		$product      = $full_order['products'][ $item_id ];
		$product_link = $product ? admin_url( 'post.php?post=' . $product->get_parent_id() . '&action=edit' ) : '';
		?>
		<td class="name with-details" data-sort-value="<?= esc_attr( $item->get_name() ) ?>">
			<?php
			/**
			 * @var WC_Product $product
			 */
			if ( $product ) {
				// SKU
				if ( $product->get_sku() ) {
					?>
                    <span class="wc-order-item-sku"><?= esc_html( $product->get_sku() ) ?></span> :
					<?php
				}

			    // NAME
                if ( $product->get_name() ) {
	                ?>
                    <span class="wc-order-item-name"><?= $product->get_name(); ?></span>
	                <?php
                }
			}

			$view_file = WC_ABSPATH . 'includes/admin/meta-boxes/views/html-order-item-meta.php';
			if ( is_readable( $view_file ) ) {
				include( $view_file );
			}
			?>
		</td>
	<?php
	}
	?>
	</tbody>
</tr>



