<?php

if ( ! class_exists( 'WCRM_Extended_Customer_List' ) ) :

    /**
     * Class WCRM_Extended_Customer_List
     *
     * Text Domain: woocommerce-report-modifier
     *
     * @property-read   boolean $initiated      have we been initiated  yet?
     * @property-read   string  $order          sort order direction
     * @property-read   string  $order_by       sort order field
     */
    class  WCRM_Extended_Customer_List extends WP_List_Table {
        private $order_by;
        private $order;

        /**
         * Constructor.
         */
        public function __construct() {

            parent::__construct( array(
                'singular' => __( 'Customer', 'woocommerce-report-modifier' ),
                'plural'   => __( 'Customers', 'woocommerce-report-modifier' ),
                'ajax'     => false,
            ) );

            $this->order_by = ( !empty( $_REQUEST[ 'orderby' ] ) ) ? $_REQUEST[ 'orderby' ] : 'last_name';
            $this->order = ( !empty( $_REQUEST[ 'order' ] ) ) ? $_REQUEST[ 'order' ] : 'DESC'; //If no order, default to desc
        }

        /**
         * Set the WP_User_Query Order By Params
         *
         * @param $query_params
         */
        public function add_user_query_order_by_params( $query_params ) {
            $query_params[ 'order' ] = $this->order;

            switch ( $this->order_by ) {
                case 'last_name':
                    break;

	            case 'email':
		            $query_params[ 'orderby' ] = 'user_email';
		            break;

	            case 'id':
                    $query_params[ 'orderby' ] = 'ID';
                    break;

                case 'orders':
                    $query_params[ 'orderby' ] = 'meta_value_num';
                    $query_params[ 'meta_key' ] = '_order_count';
                    break;

                case 'spent':
                    $query_params[ 'orderby' ] = 'meta_value_num';
                    $query_params[ 'meta_key' ] = '_money_spent';
                    break;

	            case 'username':
		            $query_params[ 'orderby' ] = 'user_name';
		            break;

            }

            return $query_params;
        }

        /**
         * Clear any sort order filters.
         */
        private function clear_sort_order() {
            switch ( $this->order_by ) {
                case 'last_name':
                    remove_action( 'pre_user_query', array( $this, 'order_by_last_name' ) );
                    return;

	            case 'id':
                case 'email':
                case 'orders':
                case 'spent':
                    remove_filter( 'wcre_extended_customer_list_query_params', array( $this, 'add_user_query_order_by_params' ) );
                    return;
            }
        }

        /**
         * Get column value.
         *
         * @param WP_User $user
         * @param string  $column_name
         * @return string
         */
        public function column_default( $user, $column_name ) {
            global $wpdb;

            $actions = array();
            $actions[ 'view' ] = array(
                'url'    => admin_url( 'edit.php?post_type=shop_order&_customer_user=' . $user->ID ),
                'name'   => __( 'View orders', 'woocommerce-report-modifier' ),
                'action' => "view",
            );
            $actions[ 'edit' ] = array(
                'url'    => admin_url( 'user-edit.php?user_id=' . $user->ID ),
                'name'   => __( 'Edit', 'woocommerce-report-modifier' ),
                'action' => "edit",
            );

            switch ( $column_name ) {

                case 'customer_name' :
                    if ( $user->last_name && $user->first_name ) {
                        return $user->last_name . ', ' . $user->first_name;
                    } else {
                        return '-';
                    }

                case 'id' :
                    return $user->ID;

                case 'first_name' :
                    return  ! empty( $user->first_name ) ? $user->first_name : '-';

                case 'last_name' :
                    return  ! empty( $user->last_name ) ? $user->last_name : '-';


                case 'username' :
                    $action = $actions['edit'];
                    $clickable_user =
                        sprintf( '<a href="%s" data-tip="%s" alt="%s" title="%s">%s</a>',
                            esc_url( $action[ 'url' ] ),
                            esc_attr( $action[ 'name' ] ),
                            esc_attr( $action[ 'name' ] ),
                            esc_attr( $action[ 'name' ] ),
                            $user->user_login
                        );

                    return $clickable_user;

                case 'email' :
                    return '<a href="mailto:' . $user->user_email . '">' . $user->user_email . '</a>';

                case 'spent' :
                    return $user->spent;

                case 'orders' :
                    $action = $actions['view'];
                    $clickable_order =
                        sprintf( '<a href="%s" data-tip="%s" alt="%s" title="%s">%d</a>',
                            esc_url( $action[ 'url' ] ),
                            esc_attr( $action[ 'name' ] ),
                            esc_attr( $action[ 'name' ] ),
                            esc_attr( $action[ 'name' ] ),
                            $user->data->orders
                        );

                    return $clickable_order;

                case 'last_order' :
                    return $user->last_order;

                case 'user_actions' :
                    ob_start();
                    ?><p>
                    <?php
                    do_action( 'woocommerce_admin_user_actions_start', $user );

                    $actions[ 'refresh' ] = array(
                        'url'    => wp_nonce_url( add_query_arg( 'refresh', $user->ID ), 'refresh' ),
                        'name'   => __( 'Refresh stats', 'woocommerce-report-modifier' ),
                        'action' => "refresh",
                    );

                    $orders = wc_get_orders( array(
                        'limit'    => 1,
                        'status'   => array( 'wc-completed', 'wc-processing' ),
                        'customer' => array( array( 0, $user->user_email ) ),
                    ) );

                    if ( $orders ) {
                        $actions[ 'link' ] = array(
                            'url'    => wp_nonce_url( add_query_arg( 'link_orders', $user->ID ), 'link_orders' ),
                            'name'   => __( 'Link previous orders', 'woocommerce-report-modifier' ),
                            'action' => "link",
                        );
                    }

                    $actions = apply_filters( 'woocommerce_admin_user_actions', $actions, $user );

                    foreach ( $actions as $action ) {
                        printf( '<a class="button tips %s" href="%s" data-tip="%s">%s</a>', esc_attr( $action[ 'action' ] ), esc_url( $action[ 'url' ] ), esc_attr( $action[ 'name' ] ), esc_attr( $action[ 'name' ] ) );
                    }

                    do_action( 'woocommerce_admin_user_actions_end', $user );
                    ?>
                    </p><?php
                    $user_actions = ob_get_contents();
                    ob_end_clean();

                    return $user_actions;
            }

            return '';
        }

        /**
         * Output an export link.
         */
        public function display_export_button() {
            $text = __( 'Export CSV' , 'woocommerce-report-modifier' );
            $html =
                '<div class="inline_box" id="export_csv">' .
                sprintf( '<a class="export_csv_2 button" download="customer_list.csv" data-export="table" href="#" alt="%s" title="%s"><span class="dashicons dashicons-download"></span>%s</a>', $text , $text , $text ) .
                '</div>';

            echo $html;
        }

        /**
         * Get columns.
         *
         * @return array
         */
        public function get_columns() {
            $columns = array(
	            'id'            => __( 'ID', 'woocommerce-report-modifier' ),
                'customer_name' => __( 'Name (Last, First)', 'woocommerce-report-modifier' ),
                'last_name'     => __( 'Last Name', 'woocommerce-report-modifier' ),
                'first_name'    => __( 'First Name', 'woocommerce-report-modifier' ),
                'username'      => __( 'Username', 'woocommerce-report-modifier' ),
                'email'         => __( 'Email', 'woocommerce-report-modifier' ),
                'orders'        => __( 'Orders', 'woocommerce-report-modifier' ),
                'spent'         => __( 'Money Spent', 'woocommerce-report-modifier' ),
                'last_order'    => __( 'Last order', 'woocommerce-report-modifier' ),
                'user_actions'  => __( 'Actions', 'woocommerce-report-modifier' ),
            );

            return $columns;
        }

        /**
         * Get the user's last order date.
         *
         * @param $user_id
         *
         * @return string
         */
        private function get_last_order( $user_id ) {

            $orders = wc_get_orders( array(
                'limit'    => 1,
                'status'   => array( 'wc-completed', 'wc-processing' ),
                'customer' => $user_id,
            ) );

            if ( !empty( $orders ) && is_a( $orders[0] , 'WC_Order' ) ) {

	            /**
	             * @var WC_Order $order
	             */
                $order = $orders[ 0 ];

                return '<a href="' . admin_url( 'post.php?post=' . $order->get_id() . '&action=edit' ) . '">' .
                       _x( '#', 'hash before order number', 'woocommerce-report-modifier' ) . $order->get_order_number() . '</a> &ndash; ' .
                       date_i18n( get_option( 'date_format' ), strtotime( $order->order_date ) );
            }

            return '-';
        }

        /**
         * Return the sortable columns.
         *
         * The associative array should follow the format 'column_slug'=>array('sortby',true),
         * where the second property states whether the field is presorted.
         * This is frequently used to provide part of the _column_headers property.
         *
         * @see https://codex.wordpress.org/Class_Reference/WP_List_Table
         *
         * @return mixed|null|void
         */
        public function get_sortable_columns() {
            $sortable_columns = array(
	            'id'            => array( 'id', true ),      // if this is true it will sort desc by default
                'customer_name' => array( 'last_name', true ),
                'first_name'    => array( 'first_name', true ),
                'last_name'     => array( 'last_name', true ),
                'email'         => array( 'email', false ),
                'orders'        => array( 'orders', true ),     // if this is true it will sort desc by default
                'spent'         => array( 'spent', true ),      // if this is true it will sort desc by default
                'username'      => array( 'username', false ),
            );

            /**
             * FILTER: wcre_extended_customer_list_sortable_columns
             *
             * @param   array $sortable_columns The sortable column array
             * @return  array                       The modified column list
             */
            return apply_filters( 'wcre_extended_customer_list_sortable_columns', $sortable_columns );
        }

        /**
         * No items found text.
         */
        public function no_items() {
            _e( 'No customers found.', 'woocommerce-report-modifier' );
        }

        /**
         * Order users by name.
         *
         * @param WP_User_Query $query
         *
         * @return WP_User_Query
         */
        public function order_by_last_name( $query ) {
            global $wpdb;

            $s = !empty( $_REQUEST[ 's' ] ) ? stripslashes( $_REQUEST[ 's' ] ) : '';

            $query->query_from .= " LEFT JOIN {$wpdb->usermeta} as meta2 ON ({$wpdb->users}.ID = meta2.user_id) ";
            $query->query_where .= " AND meta2.meta_key = 'last_name' ";
            $query->query_orderby = " ORDER BY meta2.meta_value, user_login {$this->order} ";

            if ( $s ) {
                $query->query_from .= " LEFT JOIN {$wpdb->usermeta} as meta3 ON ({$wpdb->users}.ID = meta3.user_id)";
                $query->query_where .= " AND ( user_login LIKE '%" . esc_sql( str_replace( '*', '', $s ) ) . "%' OR user_nicename LIKE '%" . esc_sql( str_replace( '*', '', $s ) ) . "%' OR meta3.meta_value LIKE '%" . esc_sql( str_replace( '*', '', $s ) ) . "%' ) ";
                $query->query_orderby = " GROUP BY ID " . $query->query_orderby;
            }

            return $query;
        }

        /**
         * Output the report.
         */
        public function output_report() {
            $this->prepare_items();

            echo '<div id="poststuff" class="woocommerce-reports-wide">';

            if ( !empty( $_GET[ 'link_orders' ] ) && wp_verify_nonce( $_REQUEST[ '_wpnonce' ], 'link_orders' ) ) {
                $linked = wc_update_new_customer_past_orders( absint( $_GET[ 'link_orders' ] ) );

                echo '<div class="updated"><p>' . sprintf( _n( '%s previous order linked', '%s previous orders linked', $linked, 'woocommerce-report-modifier' ), $linked ) . '</p></div>';
            }

            if ( !empty( $_GET[ 'refresh' ] ) && wp_verify_nonce( $_REQUEST[ '_wpnonce' ], 'refresh' ) ) {
                $user_id = absint( $_GET[ 'refresh' ] );
                $user = get_user_by( 'id', $user_id );

                delete_user_meta( $user_id, '_money_spent' );
                delete_user_meta( $user_id, '_order_count' );

                echo '<div class="updated"><p>' . sprintf( __( 'Refreshed stats for %s', 'woocommerce-report-modifier' ), $user->display_name ) . '</p></div>';
            }

            echo '<form method="post" id="woocommerce_customers_extended">';

            echo '<div class="above_table">';
            echo '<div class="inline_box">';
            $this->search_box( __( 'Search customers', 'woocommerce-report-modifier' ), 'customer_search' );
            echo '</div>';
            $this->display_export_button();
            echo '</div>';
            $this->display();
            echo '</form>';
            echo '</div>';
        }

        /**
         * Override row actions.
         *
         * @param object $item
         * @param string $column_name
         * @param string $primary
         * @return null
         */
        public function handle_row_actions( $item, $column_name, $primary ) {
            return;
        }

        /**
         * Set the sort order for performing the actual sort.
         */
        private function set_sort_order() {
            switch ( $this->order_by ) {
                case 'last_name':
                    add_action( 'pre_user_query', array( $this, 'order_by_last_name' ) );
                    return;

                case 'id':
                case 'email':
                case 'orders':
                case 'spent':
	            case 'username':
                    add_filter( 'wcre_extended_customer_list_query_params', array( $this, 'add_user_query_order_by_params' ) );
                    return;
            }
        }

        /**
         * Prepare customer list items.
         */
        public function prepare_items() {
            $current_page = absint( $this->get_pagenum() );
            $per_page = $this->get_items_per_page( 'customers_per_page' , 20 );

            $this->_column_headers = $this->get_column_info();

            $this->set_sort_order();

            /**
             * Get users.
             */
            $admin_users = new WP_User_Query(
                array(
                    'role'   => 'administrator1',
                    'fields' => 'ID',
                )
            );

            $manager_users = new WP_User_Query(
                array(
                    'role'   => 'shop_manager',
                    'fields' => 'ID',
                )
            );

            $user_query_params = array(
                'exclude' => array_merge( $admin_users->get_results(), $manager_users->get_results() ),
                'number'  => $per_page,
                'offset'  => ( $current_page - 1 ) * $per_page,
            );
            $user_query_params = apply_filters( 'wcre_extended_customer_list_query_params', $user_query_params );

            $query = new WP_User_Query( $user_query_params );

            $this->items = $query->get_results();

            $this->clear_sort_order();

            /**
             * Add WooCommerce data to user data
             */
            foreach ( $this->items as $user ) {
                $user->data->spent = wc_price( wc_get_customer_total_spent( $user->ID ) );
                $user->data->orders = wc_get_customer_order_count( $user->ID );
                $user->data->last_order = $this->get_last_order( $user->ID );
            }

            /**
             * Pagination.
             */
            $this->set_pagination_args( array(
                'total_items' => $query->total_users,
                'per_page'    => $per_page,
                'total_pages' => ceil( $query->total_users / $per_page ),
            ) );
        }
    }

    // Register to the main class objects table, make this a singleton via a simple require.
	global $wcreportmodifier;
	if ( empty( $wcreportmodifier->objects['Extended_Customer_List'] ) ) {
		$wcreportmodifier->objects['Extended_Customer_List'] = new WCRM_Extended_Customer_List();
	}
endif;
