<?php
defined( 'ABSPATH' ) or exit;

if ( ! class_exists( 'WCRM_Order_Details' ) ) {

	/**
	 * Class WCRM_Order_Details
	 *
	 *
	 * The Order Items Are In Here (for VB typically just one item)
	 * SELECT * FROM `wp_woocommerce_order_items` WHERE `order_id`='2924'
	 *
	 * The Add On Data Is In Here
	 * SELECT * FROM `wp_woocommerce_order_itemmeta` WHERE `order_item_id`='657'
	 *
	 * The main order itself is in the posts table.
	 * The order_items are the high-level products.
	 * The order_itemmeta is the product details including add ons.
	 *
	 * Text Domain: woocommerce-report-modifier
     *
     * @property-read   array       $order_list     List of full orders
	 */
	class WCRM_Order_Details  extends WC_Admin_Report {
	    private $order_list;
	    private $report_data;
	    private $sort_by = 'name';


		/**
		 * Get report data.
		 * @return stdClass
		 */
		public function get_report_data() {
            $this->sort_by = ! empty( $_REQUEST[ 'order' ] ) ? $_REQUEST[ 'order' ] : 'name';

            if ( empty( $this->report_data ) ) {
				$this->query_report_data();
			}
			return $this->report_data;
		}

		/**
		 * Output the details report.
         *
         * @used-by output_report
		 */
	    private function output_addon_details() {
	        $data = $this->get_report_data();
	        if ( ! empty( $data ) && ! empty( $data->orders ) ) {

	            // Build Full Orders for advanced sorting
                $this->build_order_list( $data->orders );

                // Sort based on options
                $sorted_list = $this->order_list;
                array_multisort( $this->order_sort, SORT_ASC , $sorted_list );

	            // Now show the details...
                $current_header = '';
	            foreach ( $sorted_list as $idx_key => $full_order ) {
		            if ( empty( $full_order[ $this->sort_by ] ) ) continue;
                    global $post , $thepostid , $theorder;
                    $post = get_post( $full_order['id'] );
                    if ( ! is_object( $post ) ) continue;
                    $thepostid = $post->ID; // for data
                    $order = $full_order['order'];
                    include( WCReportModifier_PATH . 'include/module/reports/view_Order_Details.php' );
                }
	        }
        }

        /**
         * Build the order list for this report.
         * @param array $the_orders
         */
        private function build_order_list( $the_orders ) {
            foreach ( $the_orders as $order ) {
                $order_meta[ 'id'         ] = $order->ID;
                $order_meta[ 'order'      ] = wc_get_order( $order->ID );
                $order_meta[ 'line_items' ] = $order_meta[ 'order' ]->get_items( apply_filters( 'woocommerce_admin_order_item_types' , 'line_item' ) );

                foreach ( $order_meta[ 'line_items' ] as $item_id => $item ) {
                    $key = ( $this->sort_by === 'id' ) ? $order->ID : $order->ID . '-' . $item_id;

                    $order_meta[ 'products'          ][ $item_id ] = $item->get_product();
                    $order_meta[ 'first_product_sku' ] = $order_meta[ 'products' ][ $item_id ]->get_sku();
	                $order_meta[ 'name'              ] = $order_meta[ 'products' ][ $item_id ]->get_name() . ' (' . $order_meta[ 'products' ][ $item_id ]->get_sku() . ')';

	                $this->order_list[ $key ] = $order_meta;
	                $this->order_sort[ $key ] = $this->order_list[ $key ][ $this->sort_by ];
                }
            }
        }

		/**
		 * Display the sort submenu.
		 *
		 * @used-by view_Order_Details_wrapper.php
		 */
        private function display_sort_submenu() {
        	if ( empty( $this->reports ) ) return;
        	?>
	        <ul class="subsubsub">
		        <?php
		        foreach ( $this->reports as $slug => $options ) {
		            $class = ( $slug == $this->sort_by ) ? 'active' : '';
					?>
			        <li>
				        <a href="<?= $this->reports[ $slug ][ 'url' ] ?>" class="<?= $class ?>" title="<?= $this->reports[ $slug ][ 'link_text' ] ?>"><?= $options['menu_text'] ?></a>
                    </li>
			        <?php
		        }
		        ?>
	        </ul>
            <?php
        }

		/**
		 * Output the report.
         *
         * @uses output_addon_details
		 */
		public function output_report() {
			$ranges = array(
				'year'         => __( 'Year', 'woocommerce-report-modifier' ),
				'last_month'   => __( 'Last Month', 'woocommerce-report-modifier' ),
				'month'        => __( 'This Month', 'woocommerce-report-modifier' ),
				'7day'         => __( 'Last 7 Days', 'woocommerce-report-modifier' )
			);

			$report_link = admin_url( 'admin.php?page=wc-reports&tab=orders&report=wcrm_order_details&range=' . $_GET['range'] );
			$this->reports = array(
				'id' => array(
					'url' => esc_url( add_query_arg( 'order' , 'id' , $report_link ) ),
					'link_text' => __( 'sort by order id' , 'woocommerce-report-modifier' ),
					'menu_text' => __( 'Order ID' , 'woocommerce-report-modifier' ),
				),
				'first_product_sku' => array(
					'url' => esc_url( add_query_arg( 'order' , 'first_product_sku' , $report_link ) ),
					'link_text' => __( 'sort by sku' , 'woocommerce-report-modifier' ) ,
				    'menu_text' => __( 'SKU' , 'woocommerce-report-modifier' ),
				),
				'name' => array(
					'url' => esc_url( add_query_arg( 'order' , 'name' , $report_link ) ),
					'link_text' => __( 'sort by product name' , 'woocommerce-report-modifier' ) ,
					'menu_text' => __( 'Product Name' , 'woocommerce-report-modifier' ),
				),

			);

			$current_range = ! empty( $_GET['range'] ) ? sanitize_text_field( $_GET['range'] ) : '7day';

			if ( ! in_array( $current_range, array( 'custom', 'year', 'last_month', 'month', '7day' ) ) ) {
				$current_range = '7day';
			}

			$this->calculate_current_range( $current_range );

			include( WCReportModifier_PATH . 'include/module/reports/view_Order_Details_wrapper.php' );
		}

		/**
		 * Get all data needed for this report and store in the class.
		 */
		private function query_report_data() {
			$this->report_data = new stdClass;

			$this->report_data->orders = (array) $this->get_order_report_data( array(
				'data' => array(
					'ID'    => array( 'type' => 'post_data' , 'name' => 'ID'        , 'function' => '', 'distinct' => true ),
				),
				'order_by'            => 'post_date ASC',
				'query_type'          => 'get_results',
				'filter_range'        => true,
				'order_types'         => wc_get_order_types( 'reports' ),
				'order_status'        => array( 'completed', 'processing', 'on-hold', 'refunded' )
			) );
		}

	}

	global $wcreportmodifier;
	if ( empty( $wcreportmodifier->objects['Order_Details'] ) ) {
		$wcreportmodifier->objects['Order_Details'] = new WCRM_Order_Details();
	}
}
