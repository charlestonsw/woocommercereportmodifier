<?php

if ( ! class_exists( 'WCRM_Email' ) ) :
	require_once( WCReportModifier_PATH . 'include/base/WCRM_Object.php');

	/**
	 * Class WCRM_Products
	 *
	 * Text Domain: woocommerce-report-modifier
	 *
	 */
	class  WCRM_Email extends WCRM_Object  {

		/**
		 * Things we do when we start.
		 */
		public function initialize() {
			$this->init_for_all();
			$this->init_for_admin();
		}

		/**
		 * Things we do at start for admin users only.
		 */
		private function init_for_admin() {
			if ( ! is_admin() ) {
				return;
			}
			$this->add_stop_password_change_checkbox_to_settings();
		}

		/**
		 * Things do at start for all users.
		 */
		private function init_for_all() {
			// Remove WP Password Change Notification If Enabled.
			//
			if ( 'no' === get_option( 'wcrm_stop_password_notifications' ) ) {
				remove_action( 'after_password_reset', 'wp_password_change_notification' );
			}
		}

		/**
		 * Set filter to add related products checkbox to WooCommerce products settings page.
		 */
		private function add_stop_password_change_checkbox_to_settings() {
			add_filter( 'woocommerce_email_settings' , array( $this , 'add_stop_password_change_checkbox' ) );
		}

		/**
		 * Modify the product settings array with the woocommerce_product_settings filter.
		 *
		 * @param   array $settings
		 * @return  array
		 */
		public function add_stop_password_change_checkbox( $settings ) {

			$new_settings = array();
			$settings_added = false;
			foreach ( $settings as $setting ) {
				if ( ! empty( $setting['id'] ) && ( $setting['id'] === 'email_options' ) && ! $settings_added ) {
					$new_settings[] =  array( 'title' => __( 'Email Processing Options', 'woocommerce-report-modifier' ), 'type' => 'title', 'desc' => '', 'id' => 'email_processing_options' );


					$new_settings[] = array(
						'title'         => __( 'Stop Password Change Email', 'woocommerce-report-modifier' ),
						'desc'          => __( 'Stop the site admin email whenever a user changes their password.', 'woocommerce-report-modifier' ),
						'id'            => 'wcrm_stop_password_notifications',
						'default'       => 'no',
						'type'          => 'checkbox',
					);

					$new_settings[] = array( 'type' => 'sectionend', 'id' => 'email_processing_options' );


					$settings_added = true;
				}
				$new_settings[] = $setting;
			}

			return $new_settings;
		}
	}

	global $wcreportmodifier;
	if ( empty( $wcreportmodifier->objects['Email'] ) ) {
		$wcreportmodifier->objects['Email'] = new WCRM_Products();
	}
endif;
