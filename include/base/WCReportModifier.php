<?php
defined( 'ABSPATH' ) or die( 'You cannot access this page directly.' );

/**
 * WooCommerce Report Modifier
 *
 * Functions used to modify the standard WC reports.
 *
 * Text Domain: woocommerce-report-modifier
 */
if ( ! class_exists( 'WCReportModifier' ) ) :

	/**
	 * Class WCReportModifier
	 *
	 * @property-read   array                        $objects                        A named array of our instantiated objects, the key is the class name the value is the object itself.
	 *
	 * @property        WCRM__Extended_Customer_List $Extended_Customer_List
	 * @property        WCRM_Order_Details           $Order_Details
	 */
	class  WCReportModifier {
	    private $objects;
	    protected static $_instance = null;


	    /**
	     * Main WCRM Instance.
	     *
	     * Ensures only one instance of WCRM is loaded or can be loaded.
	     *
	     * @since 4.5
	     * @static
	     * @see WCReportModifier()
	     * @return WCReportModifier - Main instance.
	     */
	    public static function instance() {
	        if ( is_null( self::$_instance ) ) {
	            self::$_instance = new self();
	        }
	        return self::$_instance;
	    }

		/**
		 * Get the value, running it through a filter.
		 *
		 * @param string $property
		 *
		 * @return mixed     null if not set or the value
		 */
		function __get( $property ) {

			if ( array_key_exists( $property , $this->objects ) && isset( $this->objects[ $property ] ) ) {
				return $this->objects[ $property ];
			}

			return null;
		}

		/**
		 * Allow isset to be called on private properties.
		 *
		 * @param $property
		 *
		 * @return bool
		 */
		public function __isset( $property ) {

			if ( is_array( $this->objects) && array_key_exists( $property , $this->objects ) && ! empty( $this->objects[ $property ] )  && is_object( $this->objects[ $property ] ) ) {
				return true;
			}

			return false;
		}

	    /**
	     * Enqueue styles.
	     */
	    public function admin_styles() {
	        wp_register_style( 'wcrm_admin_styles', $this->plugin_url() . '/css/admin.css' );
	        wp_enqueue_style( 'wcrm_admin_styles' );

	        wp_register_script( 'wcrm_admin_scripts' , $this->plugin_url() . '/js/reports.js' , array( 'jquery' ) );
	        wp_enqueue_script( 'wcrm_admin_scripts' );
	    }

		/**
		* Do this on WP Init
		*/
		public function init() {
	        $this->init_hooks();
		}

		/**
		* Setup hooks and filters for this plugin.
		*/
		private function init_hooks() {
			if ( is_admin() ) {
				$this->set_screen_options();
				add_action( 'admin_enqueue_scripts', array( $this, 'admin_styles' ) );
				add_filter( 'plugin_row_meta', array( $this, 'plugin_row_meta' ), 10, 2 );
				add_filter( 'woocommerce_reports_get_order_report_data_args', array( $this, 'change_limit' ) );
				add_filter( 'woocommerce_admin_reports', array( $this, 'add_extended_customer_report_to_menu' ) );
				$this->add_the_addons_report();
			}

			require_once( WCReportModifier_PATH . 'include/module/products/WCRM_Products.php' );
			require_once( WCReportModifier_PATH . 'include/module/email/WCRM_Email.php' );
		}

	    /**
	     * Add the extended customer report to the menu.
	     *
	     * @param $reports
	     * @return mixed
	     */
	    public function add_extended_customer_report_to_menu( $reports ) {
	        $reports[ 'customers' ][ 'reports' ]['extended_customer_list'] = array(
	            'title'         => __( 'Extended List' , 'woocommerce-report-modifier' ),
	            'description'   => '',
	            'hide_title'    => true,
	            'callback'      => array( $this , 'get_report' )
	        );
	        return $reports;
	    }

	    /**
	     * Add screen options to all WooCommerce Reports pages.
	     */
	    public function add_extended_customer_report_options( ) {
	        $args = array(
	            'label' => __( 'Customers' , 'woocommerce-report-modifier' ),
	            'default' => 20,
	            'option' => 'customers_per_page',
	        );
	        add_screen_option( 'per_page' , $args );

	        require_once( WCReportModifier_PATH . 'include/module/reports/WCRM_Extended_Customer_List.php' );
	    }

		/**
		 * Add order add ons report to the menu.
		 *
		 * @param $reports
		 * @return mixed
		 */
		public function add_order_addon_report_to_menu( $reports ) {
			$reports[ 'orders' ][ 'reports' ]['wcrm_order_details'] = array(
				'title'         => __( 'Order Details' , 'woocommerce-report-modifier' ),
				'description'   => '',
				'hide_title'    => true,
				'callback'      => array( $this , 'get_report' )
			);
			return $reports;
		}

		/**
		 * Add the add ons report to the order menu.
		 */
	    private function add_the_addons_report() {
	    	// TODO: Test if add ons is active
		    // if ( WC addons is active ) {
		    add_filter( 'woocommerce_admin_reports', array( $this, 'add_order_addon_report_to_menu' ) );
	    }

		/**
		* Change WooCommerce report limits form 12 to 30.
		*/
		public function change_limit( $args ) {
			if ( isset( $args['limit'] ) ) {
				$args['limit'] = 30;
			}
			return $args;
		}

	    /**
	     * Get a report from our reports subfolder.
	     *
	     * @param string $name
	     */
	    public function get_report( $name ) {
	    	switch ( $name ) {
			    case 'extended_customer_list':
				    require_once( WCReportModifier_PATH . 'include/module/reports/WCRM_Extended_Customer_List.php' );
				    $this->Extended_Customer_List->output_report();
			    	break;

			    case 'wcrm_order_details':
				    require_once( WCReportModifier_PATH . 'include/module/reports/WCRM_Order_Details.php' );
				    $this->Order_Details->output_report();
			    	break;
		    }
	    }

	    /**
	     * Get a class name from a given name.
	     *
	     * @param $name
	     *
	     * @return string
	     */
	    private function get_classname( $name ) {
	        $name = sanitize_title( str_replace( '_', '-', $name ) );
	        $name = ucwords(str_replace( '-' , ' ', $name ));
	        return 'WC_Report_' . str_replace( ' ', '_', $name );
	    }

		/**
		 * Show row meta on the plugin screen.
		 *
		 * @param	mixed $links Plugin Row Meta
		 * @param	mixed $file  Plugin Base file
		 * @return	array
		 */
		public function plugin_row_meta( $links, $file ) {
			if ( $file == $this->plugin_basename() ) {
				$row_meta = array(
					'docs'    => '<a target="wcrm_docs" href="' . esc_url( apply_filters( 'wcrm_docs_url', 'http://docs.storelocatorplus.com/wcrm/' ) ) . '" title="' . esc_attr( __( 'View WCRM Documentation', 'woocommerce-report-modifier' ) ) . '">' . __( 'Docs', 'woocommerce-report-modifier' ) . '</a>',
				);

				return array_merge( $links, $row_meta );
			}

			return (array) $links;
		}

	    /**
	     * Get the plugin url.
	     * @return string
	     */
	    public function plugin_url() {
	        return untrailingslashit( plugins_url( '/', WCReportModifier_FILE ) );
	    }

		/**
		 * Get the plugin basename.
		 * @return string
		 */
	    public function plugin_basename() {
		    return plugin_basename( WCReportModifier_FILE );
	    }

	    /**
	     * Set screen options for specific WC pages.
	     */
	    private function set_screen_options() {
	        $request['report'] = isset( $_REQUEST['report'] ) ?  $_REQUEST['report'] : '';
	        $request['tab'] = isset( $_REQUEST['tab'] ) ?  $_REQUEST['tab'] : '';

	        if ( $request['tab'] === 'customers' ) {
	            switch ( $request['report'] ) {
	                case 'extended_customer_list':
	                    add_action( 'load-woocommerce_page_wc-reports', array( $this, 'add_extended_customer_report_options' ) );
	                    add_filter( 'set-screen-option' , array( $this , 'save_screen_option' ) , 10 , 3 );
	                    break;
	            }
	        }

	    }

	    /**
	     * Save screen options to user meta.
	     *
	     * @param mixed $status
	     * @param string $option
	     * @param mixed $value
	     * @return mixed
	     */
	    public function save_screen_option( $status, $option , $value ) {
	        return $value;
	    }
	}
endif;
