<?php

if ( ! class_exists( 'WCRM_Object' ) ) :

/**
 * Class WCRM_Object
 *
 * @package     WooCommerce/ReportModifier
 * @author Lance Cleveland <lance@charlestonsw.com>
 * @copyright 2016 Charleston Software Associates, LLC
 * @version     1.2
 *
 */
class WCRM_Object {

	/**
	 * @param array $options
	 */
	function __construct( $options = array() ) {

		if ( is_array( $options ) && ! empty( $options ) ) {
			foreach ( $options as $property => $value ) {
				if ( property_exists( $this, $property ) ) {
					$this->$property = $value;
				}
			}
		}
		$this->initialize();
	}

	/**
	 * Do these things when this object is invoked.
	 */
	protected function initialize() {
		// Override with anything you want to run when your extension is invoked.
	}
}

endif;

